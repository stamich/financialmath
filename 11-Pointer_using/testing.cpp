//
// Created by EB79OJ on 2019-05-07.
//

#include "testing.h"

/*  Whether debug messages are enabled */
static bool debugEnabled = false;

bool isDebugEnabled() {
    return debugEnabled;
}

void setDebugEnabled( bool enable ) {
    debugEnabled = enable;
}
