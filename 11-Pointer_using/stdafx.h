//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_STDAFX_H
#define FINANCIALMATH_STDAFX_H

#pragma once


#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "testing.h"
#include <memory>
#include <cstring>

#ifdef __unix__
#include <tr1/memory> // On Unix shared_ptr is in <tr1/memory>
#endif

#endif //FINANCIALMATH_STDAFX_H
