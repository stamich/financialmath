//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_CALLOPTION_H
#define FINANCIALMATH_CALLOPTION_H

#pragma once

#include "stdafx.h"
#include "BlackScholesModel.h"

class CallOption {
public:
    CallOption();
    double strike;
    double maturity;

    double payoff( double stockAtMaturity ) const;

    double price( const BlackScholesModel& bsm )
    const;
};

void testCallOption();

#endif //FINANCIALMATH_CALLOPTION_H
