//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_PUTOPTION_H
#define FINANCIALMATH_PUTOPTION_H

#pragma once

#include "stdafx.h"
#include "BlackScholesModel.h"

class PutOption {
public:
    PutOption();
    double strike;
    double maturity;

    double payoff( double stockAtMaturity ) const;

    double price( const BlackScholesModel& bsm )
    const;
};

void testPutOption();

#endif //FINANCIALMATH_PUTOPTION_H
