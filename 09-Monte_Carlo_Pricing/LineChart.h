//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_LINECHART_H
#define FINANCIALMATH_LINECHART_H

#pragma once

#include "stdafx.h"

class LineChart {
public:
    LineChart();
    void setTitle( const std::string& title );
    void setSeries( const std::vector<double>& x,
                    const std::vector<double>& y );
    void writeAsHTML( std::ostream& out ) const;
    void writeAsHTML( const std::string& file ) const;
private:
    std::string title;
    std::vector<double> x;
    std::vector<double> y;
};


void testLineChart();

#endif //FINANCIALMATH_LINECHART_H
