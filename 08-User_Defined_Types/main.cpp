//
// Created by EB79OJ on 2019-04-26.
//
#include "matlib.h"
#include "geometry.h"
#include "CallOption.h"
#include "PieChart.h"
#include "BlackScholesModel.h"

using namespace std;

int main() {
    testMatlib();
    testGeometry();
    testPieChart();
    testCallOption();
    testBlackScholesModel();
}
