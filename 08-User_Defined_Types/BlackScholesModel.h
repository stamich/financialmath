//
// Created by EB79OJ on 2019-04-26.
//

#ifndef FINANCIALMATH_BLACKSCHOLESMODEL_H
#define FINANCIALMATH_BLACKSCHOLESMODEL_H

#pragma once

#include "stdafx.h"

class BlackScholesModel {
public:
    double stockPrice;
    double volatility;
    double riskFreeRate;
    double date;
};

void testBlackScholesModel();

#endif //FINANCIALMATH_BLACKSCHOLESMODEL_H
