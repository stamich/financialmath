//
// Created by EB79OJ on 2019-04-26.
//

#ifndef FINANCIALMATH_PIECHART_H
#define FINANCIALMATH_PIECHART_H

#pragma once

#include "stdafx.h"

class PieChart {
public:
    void setTitle( const std::string& title );
    void addEntry( const std::string& label,
                   double value );
    void writeAsHTML( std::ostream& out ) const;
    void writeAsHTML(const std::string& file ) const;
private:
    std::string title;
    std::vector<std::string> labels;
    std::vector<double> values;
};

void testPieChart();

#endif //FINANCIALMATH_PIECHART_H
