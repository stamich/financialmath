//
// Created by EB79OJ on 2019-04-26.
//

#ifndef FINANCIALMATH_MATLIB_H
#define FINANCIALMATH_MATLIB_H

#pragma once

#include "stdafx.h"

/**
 *  Computes the cumulative
 *  distribution function of the
 *  normal distribution
 */
double normcdf( double x );

/**
 *  Computes the inverse of normcdf
 */
double norminv( double x );

/**
 *  Test function
 */
void testMatlib();

#endif //FINANCIALMATH_MATLIB_H
