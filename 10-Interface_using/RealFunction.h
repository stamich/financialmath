//
// Created by EB79OJ on 2019-05-07.
//

#ifndef FINANCIALMATH_REALFUNCTION_H
#define FINANCIALMATH_REALFUNCTION_H

#pragma once

/**
 *   A real function is something that can
 *   be evaluated at a real number to give
 *   a real number
 */
class RealFunction {
public:
    /*  A virtual destructor */
    virtual ~RealFunction() {};
    /*  This method is abstract, there is
        no definition */
    virtual double evaluate( double x ) = 0;
};

#endif //FINANCIALMATH_REALFUNCTION_H
