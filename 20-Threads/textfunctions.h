//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_TEXTFUNCTIONS_H
#define FINANCIALMATH_TEXTFUNCTIONS_H

#pragma once

#include "stdafx.h"

// Functions for manipulating text

/**
 *  Replace all quote characters etc in a
 *  string with the correct escape characters
 *  to place into Javascript
 */
std::string escapeJavascriptString( const std::string& in );

void testTextFunctions();

#endif //FINANCIALMATH_TEXTFUNCTIONS_H
