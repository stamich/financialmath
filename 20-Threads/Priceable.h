//
// Created by EB79OJ on 2019-05-08.
//

#ifndef FINANCIALMATH_PRICEABLE_H
#define FINANCIALMATH_PRICEABLE_H

#pragma once

#include "MultiStockModel.h"

/**
 *   Represents a general security
 */
class Priceable {
public:
    /*  Compute the price of the security  */
    virtual double price(
            const MultiStockModel& model ) const = 0;
};

#endif //FINANCIALMATH_PRICEABLE_H
