//
// Created by EB79OJ on 2019-05-08.
//

#ifndef FINANCIALMATH_TASK_H
#define FINANCIALMATH_TASK_H

#pragma once

#include "stdafx.h"

/**
 *   A task is simply something with an execute
 *   method.
 */
class Task {
public:
    virtual ~Task() {}
    virtual void execute() = 0;
};

#endif //FINANCIALMATH_TASK_H
