//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_BLACKSCHOLESMODEL_H
#define FINANCIALMATH_BLACKSCHOLESMODEL_H

#pragma once

#include "stdafx.h"
#include "Matrix.h"

class BlackScholesModel {
public:
    BlackScholesModel();
    double drift;
    double stockPrice;
    double volatility;
    double riskFreeRate;
    double date;

    Matrix generatePricePaths(
            std::mt19937& rng,
            double toDate,
            int nPaths,
            int nSteps) const;

    Matrix generateRiskNeutralPricePaths(
            std::mt19937& rng,
            double toDate,
            int nPaths,
            int nSteps) const;

};

//
//   Tests
//

void testBlackScholesModel();

#endif //FINANCIALMATH_BLACKSCHOLESMODEL_H
