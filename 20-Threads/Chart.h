//
// Created by EB79OJ on 2019-05-08.
//

#ifndef FINANCIALMATH_CHART_H
#define FINANCIALMATH_CHART_H

#include <string>

class Chart {
public:
    void setTitle( const std::string title ) {
        this->title = title;
    }
private:
    std::string title;
};

#endif //FINANCIALMATH_CHART_H
