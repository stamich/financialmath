//
// Created by EB79OJ on 2019-04-25.
//

#ifndef FINANCIALMATH_CALLOPTION_H
#define FINANCIALMATH_CALLOPTION_H

#pragma once

#include "stdafx.h"
#include "MultiStockModel.h"
#include "PathIndependentOption.h"

class CallOption : public PathIndependentOption {
public:

    /*  Returns the payoff at maturity given a column vector
        of scenarios */
    Matrix payoffAtMaturity( const Matrix& stockAtMaturity ) const;

    double price( const MultiStockModel& bsm )
    const;
};

void testCallOption();
#endif //FINANCIALMATH_CALLOPTION_H
