//
// Created by EB79OJ on 2019-05-08.
//

#ifndef FINANCIALMATH_KNOCKOUTOPTION_H
#define FINANCIALMATH_KNOCKOUTOPTION_H

#pragma once

#include "ContinuousTimeOptionBase.h"

/**
 *   An option with a barrier
 */
class KnockoutOption : public ContinuousTimeOptionBase {
public:
    virtual ~KnockoutOption() {}

    double getBarrier() const {
        return barrier;
    }

    void setBarrier(double barrier) {
        this->barrier=barrier;
    }

    bool isPathDependent() const {
        return true;
    }
private:
    double barrier;
};

#endif //FINANCIALMATH_KNOCKOUTOPTION_H
