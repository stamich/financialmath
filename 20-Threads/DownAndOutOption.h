//
// Created by EB79OJ on 2019-05-08.
//

#ifndef FINANCIALMATH_DOWNANDOUTOPTION_H
#define FINANCIALMATH_DOWNANDOUTOPTION_H

#pragma once

#include "KnockoutOption.h"

class DownAndOutOption : public KnockoutOption {
public:
    Matrix payoff(
            const Matrix& prices ) const;
};


void testDownAndOutOption();

#endif //FINANCIALMATH_DOWNANDOUTOPTION_H
