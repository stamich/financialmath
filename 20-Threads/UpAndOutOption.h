//
// Created by EB79OJ on 2019-05-08.
//

#ifndef FINANCIALMATH_UPANDOUTOPTION_H
#define FINANCIALMATH_UPANDOUTOPTION_H

#pragma once

#include "KnockoutOption.h"

class UpAndOutOption : public KnockoutOption {
public:
    Matrix payoff(
            const Matrix& prices ) const;
};

typedef std::shared_ptr<UpAndOutOption> SPUpAndOutOption;
typedef std::shared_ptr<const UpAndOutOption> SPCUpAndOutOption;


void testUpAndOutOption();

#endif //FINANCIALMATH_UPANDOUTOPTION_H
